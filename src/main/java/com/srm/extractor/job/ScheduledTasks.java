/**
 * 
 */
package com.srm.extractor.job;

import java.text.SimpleDateFormat;
import java.util.Iterator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.srm.extractor.db.repo.BookRepository;
import com.srm.extractor.db.service.SequenceGeneratorService;
import com.srm.extractor.json.Book;
import com.srm.extractor.json.Hits;

/**
 * @author sebastian
 *
 */
@Component
public class ScheduledTasks {
	
	private static final Logger LOGGER = LogManager.getLogger(ScheduledTasks.class);

	private static SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
	@Autowired
	RestTemplateBuilder builder;
	
	@Autowired
	BookRepository repo;
	
	@Autowired
	SequenceGeneratorService sequenceGenerator;
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}

	@Scheduled(cron = "* 0/1 * * * *")
	public void task() {
		LOGGER.info("------------------------------------------------");
		LOGGER.info("INICIO JOB AT: "+ sdf.format(System.currentTimeMillis()));
		Hits consumoWS = this.consumoWS();
		
		int i = insertoDB(consumoWS);
		
		LOGGER.info("------------------------");
		LOGGER.info("QTY REGISTROS INSERTADOS: " + i);
		LOGGER.info("FIN AT:  " + sdf.format(System.currentTimeMillis()));
		LOGGER.info("------------------------------------------------");
	}
	
	
	private final Hits consumoWS() {
		
			RestTemplate restTemplate = builder.build();
			
			Hits resp = restTemplate.getForObject("https://hn.algolia.com/api/v1/search_by_date?query=java", Hits.class);
			
			return resp;
		
	}

	private final int insertoDB(Hits hits){
		
		int i = 0;
		
		if (hits != null && !hits.getHits().isEmpty()) {
			Iterator<Book> iterator = hits.getHits().iterator();
			
			while (iterator.hasNext()) {
				Book next = iterator.next();
							
				next.setId(sequenceGenerator.generateSequence(next.SEQUENCE_NAME));
				repo.insert(next);
				
				i++;
								
			}
			
		}
		return i;
	}
		
}
