/**
 * 
 */
package com.srm.extractor.json;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author sebastian
 *
 */
@Document(collection = "books")
public class Book {
	
    @Transient
    public static final String SEQUENCE_NAME = "books_sequence";

    @Id
    private long id;
	
	@JsonProperty("created_at")
	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	@Field(targetType =  FieldType.DATE_TIME)
	private Date created_at;
	
	@JsonProperty("title")
	private String title;
	
	@JsonProperty("url")
	private String url;
	
	@JsonProperty("author")
	private String author;
	
	@JsonProperty("points")
	private String points;

	@JsonProperty("story_text")
	private String story_text;
	
	@JsonProperty("comment_text")
	private String comment_text;
	
	@JsonProperty("num_comments")
	private String num_comments;
	
	@JsonProperty("story_id")
	private BigInteger story_id;
	
	@JsonProperty("story_title")
	private String story_title;
	
	@JsonProperty("story_url")
	private String story_url;
	
	@JsonProperty("parent_id")
	private BigInteger parent_id;
	
	@JsonProperty("created_at_i")
	private BigInteger created_at_i;
	
	@JsonProperty("_tags")
	private ArrayList<String> tags;
	
	@JsonProperty("objectID")
	private BigInteger objectID;
	
	@JsonProperty("_highlightResult")
	private HighlightResult highlightResult;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPoints() {
		return points;
	}

	public void setPoints(String points) {
		this.points = points;
	}

	public String getStory_text() {
		return story_text;
	}

	public void setStory_text(String story_text) {
		this.story_text = story_text;
	}

	public String getComment_text() {
		return comment_text;
	}

	public void setComment_text(String comment_text) {
		this.comment_text = comment_text;
	}

	public String getNum_comments() {
		return num_comments;
	}

	public void setNum_comments(String num_comments) {
		this.num_comments = num_comments;
	}

	public BigInteger getStory_id() {
		return story_id;
	}

	public void setStory_id(BigInteger story_id) {
		this.story_id = story_id;
	}

	public String getStory_title() {
		return story_title;
	}

	public void setStory_title(String story_title) {
		this.story_title = story_title;
	}

	public String getStory_url() {
		return story_url;
	}

	public void setStory_url(String story_url) {
		this.story_url = story_url;
	}

	public BigInteger getParent_id() {
		return parent_id;
	}

	public void setParent_id(BigInteger parent_id) {
		this.parent_id = parent_id;
	}

	public BigInteger getCreated_at_i() {
		return created_at_i;
	}

	public void setCreated_at_i(BigInteger created_at_i) {
		this.created_at_i = created_at_i;
	}

	public ArrayList<String> getTags() {
		return tags;
	}

	public void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}

	public BigInteger getObjectID() {
		return objectID;
	}

	public void setObjectID(BigInteger objectID) {
		this.objectID = objectID;
	}

	public HighlightResult getHighlightResult() {
		return highlightResult;
	}

	public void setHighlightResult(HighlightResult highlightResult) {
		this.highlightResult = highlightResult;
	}

}
