/**
 * 
 */
package com.srm.extractor.json;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author sebastian
 *
 */
public class CommentText {
	
	@JsonProperty("value")
	private String value;
	
	@JsonProperty("matchLevel")
	private String matchLevel;
	
	@JsonProperty("fullyHighlighted")
	private boolean fullyHighlighted;
	
	@JsonProperty("matchedWords")
	private ArrayList<String> matchedWords;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getMatchLevel() {
		return matchLevel;
	}

	public void setMatchLevel(String matchLevel) {
		this.matchLevel = matchLevel;
	}

	public boolean isFullyHighlighted() {
		return fullyHighlighted;
	}

	public void setFullyHighlighted(boolean fullyHighlighted) {
		this.fullyHighlighted = fullyHighlighted;
	}

	public ArrayList<String> getMatchedWords() {
		return matchedWords;
	}

	public void setMatchedWords(ArrayList<String> matchedWords) {
		this.matchedWords = matchedWords;
	}

	@Override
	public String toString() {
		return "CommentText [value=" + value + ", matchLevel=" + matchLevel + ", fullyHighlighted=" + fullyHighlighted
				+ ", matchedWords=" + matchedWords + "]";
	}
	
	

}
