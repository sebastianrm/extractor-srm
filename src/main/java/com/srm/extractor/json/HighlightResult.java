/**
 * 
 */
package com.srm.extractor.json;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author sebastian
 *
 */
public class HighlightResult {
	
	@JsonProperty("author")
	private Author autor;

	@JsonProperty("comment_text")
	private CommentText commentText;
	
	@JsonProperty("story_title")
	private StoryTitle storyTitle;
	
	@JsonProperty("story_url")
	private StoryUrl storyUrl;

	public Author getAutor() {
		return autor;
	}

	public void setAutor(Author autor) {
		this.autor = autor;
	}

	public CommentText getCommentText() {
		return commentText;
	}

	public void setCommentText(CommentText commentText) {
		this.commentText = commentText;
	}


	public StoryTitle getStoryTitle() {
		return storyTitle;
	}

	public void setStoryTitle(StoryTitle storyTitle) {
		this.storyTitle = storyTitle;
	}

	public StoryUrl getStoryUrl() {
		return storyUrl;
	}

	public void setStoryUrl(StoryUrl storyUrl) {
		this.storyUrl = storyUrl;
	}

	@Override
	public String toString() {
		return "HighlightResult [autor=" + autor + ", commentText=" + commentText + ", storyTitle=" + storyTitle
				+ ", storyUrl=" + storyUrl + "]";
	}
	
	
}
