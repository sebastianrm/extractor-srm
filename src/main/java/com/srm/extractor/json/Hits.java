package com.srm.extractor.json;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Hits {
	
	@JsonProperty("hits")
	private ArrayList<Book> hits;

	public ArrayList<Book> getHits() {
		return hits;
	}

	public void setHits(ArrayList<Book> hits) {
		this.hits = hits;
	}

	@Override
	public String toString() {
		return "Hits [hits=" + hits + "]";
	}

	
}
