package com.srm.extractor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class ExtractorSrmApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExtractorSrmApplication.class, args);
	}

}
