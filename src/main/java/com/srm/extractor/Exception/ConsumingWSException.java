/**
 * 
 */
package com.srm.extractor.Exception;

/**
 * @author sebastian
 *
 */
public class ConsumingWSException extends Exception {

	private static final long serialVersionUID = 1L;

	public ConsumingWSException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ConsumingWSException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ConsumingWSException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ConsumingWSException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ConsumingWSException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
	

}
