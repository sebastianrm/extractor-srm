/**
 * 
 */
package com.srm.extractor.Exception;

/**
 * @author sebastian
 *
 */
public class InserDataExcetion extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public InserDataExcetion() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public InserDataExcetion(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public InserDataExcetion(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public InserDataExcetion(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public InserDataExcetion(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
