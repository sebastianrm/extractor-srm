/**
 * 
 */
package com.srm.extractor.db.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.srm.extractor.json.Book;

/**
 * @author sebastian
 *
 */
public interface BookRepository extends MongoRepository<Book, Long> {
	

}
